package com.codeoftheweb.salvo.controller;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class SalvoController {

    @Autowired
    GameRepository gameRepository;

    @Autowired
    GamePlayerRepository gamePlayerRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    ShipRepository shipRepository;

    @Autowired
    SalvoRepository salvoRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;



    //******************************************JSON CON TODOS LOS JUEGOS***********************************************
    @RequestMapping("/games")
    public Map<String,Object> getGames(Authentication authentication){
        Map<String, Object> dto = new LinkedHashMap<>();
//        authentication = SecurityContextHolder.getContext().getAuthentication();
//        Player authenticatedPlayer = getAuthentication(authentication);
//        if (authenticatedPlayer == null)
//            dto.put("player", "Guest");
        if(isGuest(authentication)){
            dto.put("player", "Guest");
        }
        else {
            Player playerAutenticado = playerRepository.findByUserName((authentication.getName()));
            dto.put("player", playerAutenticado.playerDTO());
            //dto.put("player", authenticatedPlayer.playerDTO());
        }
        dto.put("games", gameRepository.findAll()
                                          .stream()
                                          .sorted(Comparator.comparingLong(Game::getId))
                                          .map(Game -> Game.makeGameDTO())
                                          .collect(Collectors.toList()));
        return dto;
    }

    //*************************************JSON CON INFO DE UN JUEGO ESPECIFICO*****************************************
    @RequestMapping("/game_view/{id}")
    public ResponseEntity<Object> getGameView(@PathVariable long id, Authentication authentication) {
        Player authenticatedPlayer = getAuthentication(authentication);
        GamePlayer gamePlayer = gamePlayerRepository.findById(id).get();
        if (gamePlayer.getPlayer().getId() != authenticatedPlayer.getId() ){
            return new ResponseEntity<>(MakeMap("error","This is not your Game"),HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(gamePlayer.gameViewDTO(),HttpStatus.ACCEPTED);

    }


    @RequestMapping("/leaderBoard")
    public List<Map<String,Object>> makeLeaderBoard(){
        return playerRepository
                .findAll()
                .stream()
                .sorted(Comparator.comparing(Player::getTotalscore).reversed())
                .map(player -> player.playerLeaderBoardDTO())
                .collect(Collectors.toList());
    }

    //**********************************************CREAR UN PLAYER*****************************************************
    @RequestMapping(path ="/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> createUser(@RequestParam String name,
                                                         @RequestParam String pwd){
        if (name.isEmpty()) {
            return new ResponseEntity<>(MakeMap("error","No name given"), HttpStatus.FORBIDDEN);
        }

        Player newPlayer = playerRepository.findByUserName(name);
        if (newPlayer != null) {
            return new ResponseEntity<>(MakeMap("error","Name already used"), HttpStatus.CONFLICT);
        }

        Player player = playerRepository.save(new Player(name,passwordEncoder.encode(pwd)));
        return new ResponseEntity<>(MakeMap("player",player.getUserName()), HttpStatus.CREATED);
    }

    //***********************************************CREAR UN JUEGO*****************************************************
    @RequestMapping(path ="/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> createGame(Authentication authentication){
        if (getAuthentication(authentication) == null) {
            return new ResponseEntity<>(MakeMap("error","No player logged in"), HttpStatus.FORBIDDEN);
        }
        Game game = gameRepository.save(new Game(new Date()));

        //Player player2 = playerRepository.findByUserName(authentication.getName());
        Player player = getAuthentication(authentication);

        GamePlayer gamePlayer = gamePlayerRepository.save(new GamePlayer(player,game));
        return new ResponseEntity<>(MakeMap("gpid",gamePlayer.getId()), HttpStatus.CREATED);
    }
    //**********************************************UNIRSE A UNA PARTIDA************************************************
    @RequestMapping(path ="/game/{id}/players")
    public ResponseEntity<Map<String,Object>> JoinGame(Authentication authentication,
                                                       @PathVariable long id){
        if (authentication.getName().isEmpty()) {
            return new ResponseEntity<>(MakeMap("error","No player logged in"), HttpStatus.UNAUTHORIZED);

        }
        Game game = gameRepository.findById(id).orElse(null);
        if(game == null ) {
            return new ResponseEntity<>(MakeMap("error","No such game"), HttpStatus.CONFLICT);

        }
        if (game.getGamePlayers().stream().map(gp -> gp.getPlayer().getUserName()).collect(Collectors.toList()).contains(authentication.getName()))
        {
            return new ResponseEntity<>(MakeMap("error", "User already in game"), HttpStatus.FORBIDDEN);

        }
        if(game.getGamePlayers().size() >= 2){
            return new ResponseEntity<>(MakeMap("error","Game is full"), HttpStatus.FORBIDDEN);

        }
        GamePlayer gamePlayer = gamePlayerRepository.save(new GamePlayer(getAuthentication(authentication),game));
        //game.addGamePlayer(gamePlayer);
        return new ResponseEntity<>(MakeMap("gpid",gamePlayer.getId()), HttpStatus.CREATED);

    }

    //***************************************************CREAR SHIPS****************************************************

    @RequestMapping(value = "/games/players/{id}/ships", method = RequestMethod.POST)
    private ResponseEntity<Map<String,Object>> AddShips(@PathVariable long id,
                                                        @RequestBody List<Ship> ships,
                                                        Authentication authentication) {

        GamePlayer gamePlayer = gamePlayerRepository.findById(id).orElse(null);
        Player loggedPlayer = getAuthentication(authentication);


        if (loggedPlayer == null)
            return new ResponseEntity<>(MakeMap("error", "No player logged in"), HttpStatus.UNAUTHORIZED);
        if (gamePlayer == null)
            return new ResponseEntity<>(MakeMap("error", "There is no gamePlayer with that id"), HttpStatus.UNAUTHORIZED);
        if (WrongGamePlayer(gamePlayer, loggedPlayer)) {
            return new ResponseEntity<>(MakeMap("error", "This is not your game!"), HttpStatus.UNAUTHORIZED);
        } else {
            if (gamePlayer.getShips().isEmpty()) {
                ships.forEach(ship -> ship.setGamePlayer(gamePlayer));
                //gamePlayer.setShip(ships);
                shipRepository.saveAll(ships);
                return new ResponseEntity<>(MakeMap("ok", "Ships saved"), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(MakeMap("error", "Player already has ships"), HttpStatus.FORBIDDEN);
            }
        }
    }

    //***************************************************CREAR SALVOES**************************************************
    @RequestMapping("/games/players/{id}/salvoes")
    private ResponseEntity<Map<String,Object>> AddSalvoes(@PathVariable long id,
                                                          @RequestBody Salvo salvo,
                                                          Authentication authentication) {


        GamePlayer gamePlayer = gamePlayerRepository.findById(id).orElse(null);
        Player loggedPlayer = getAuthentication(authentication);

        if (loggedPlayer == null)
            return new ResponseEntity<>(MakeMap("error", "No player logged in"), HttpStatus.UNAUTHORIZED);
        if (gamePlayer == null)
            return new ResponseEntity<>(MakeMap("error", "No such gamePlayer"), HttpStatus.FORBIDDEN);
        if (WrongGamePlayer(gamePlayer, loggedPlayer))
            return new ResponseEntity<>(MakeMap("error", "Wrong GamePlayer"), HttpStatus.FORBIDDEN);
        if (salvo.getSalvoLocations().size()> 5){
            return new ResponseEntity<>(MakeMap("error", "Wrong GamePlayer"), HttpStatus.FORBIDDEN);
        } else {
            if (!turnHasSalvoes(salvo, gamePlayer.getSalvoes())) {
                salvo.setTurn(gamePlayer.getSalvoes().size() + 1);
                salvo.setGamePlayer(gamePlayer);
                salvoRepository.save(salvo);
                return new ResponseEntity<>(MakeMap("ok", "Salvoes added"), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(MakeMap("error", "Player has fired salvoes in this turn"), HttpStatus.FORBIDDEN);
            }
        }
    }



    //************************************************METODOS AUXILIARES************************************************

    //Metodos que ayudan a los request mapping definidos
    //**********************************************ONTENER EL PLAYER LOGUEADO******************************************
    private Player getAuthentication(Authentication authentication) {
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken)
        {
            return null;
        }else   {
            return playerRepository.findByUserName((authentication.getName()));
        }
    }

    //**********************************************CREAR UN MAP********************************************************
    private  Map<String,Object> MakeMap(String key, Object value){
        Map<String,Object> map = new LinkedHashMap<>();
        map.put(key,value);
        return map;
    }

    //************************************DETERMINAR SI ES EL GP INCORRECTO*********************************************
    private boolean WrongGamePlayer(GamePlayer gamePlayer,
                                    Player player){
        boolean incorrectGP = gamePlayer.getPlayer().getId() != player.getId();
        return incorrectGP;
    }

    //*******************************DETERMINAR SI YA EXISTE SALVO CREADO INCORRECTO************************************
    private boolean turnHasSalvoes(Salvo newSalvo, Set<Salvo> playerSalvoes){
        boolean hasSalvoes = false;
        for (Salvo salvo: playerSalvoes) {
            if(salvo.getTurn() == newSalvo.getTurn()){
                hasSalvoes = true;
            }
        }
        return hasSalvoes;
    }


    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }
}
