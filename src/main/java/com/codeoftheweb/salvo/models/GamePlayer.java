package com.codeoftheweb.salvo.models;

import com.codeoftheweb.salvo.repositories.ScoreRepository;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
public class GamePlayer {

    @Autowired
    ScoreRepository scoreRepository;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private Date joinDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private Player player;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    private Set<Salvo> salvoes = new HashSet<>();

    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    private Set<Ship> ships = new HashSet<>();

    public GamePlayer() {
    }

    public GamePlayer(Player player, Game game) {
        this.joinDate = new Date();
        this.player = player;
        this.game = game;
    }

    public long getId() {
        return id;
    }

    public Date getJoinDate() {
        return joinDate;
    }


    public Player getPlayer() {
        return player;
    }

    @JsonIgnore
    public Game getGame() {
        return game;
    }

    public Set<Salvo> getSalvoes() {
        return salvoes;
    }

    public Set<Ship> getShips() {
        return ships;
    }

    public Map<String,Object> makeGamePlayerDTO(){
        Map<String,Object> dto = new LinkedHashMap<String, Object>();
        dto.put("gpid", this.getId());
        dto.put("hasShips", this.hasShips());
        dto.put("player", this.getPlayer().playerDTO());
        return dto;
    }

    // funcion del game_view
    public Map<String, Object> gameViewDTO() {
        //se determina el oponente y se lo guarda en una variable auxiliar
        GamePlayer opponent = GetOpponent().orElse(null);
        Map<String, Object> dto = new LinkedHashMap<>();

        dto.put("id", this.getGame().getId());
        dto.put("creationDate", this.getGame().getCreationDate());
        dto.put("gameState", getGameState(opponent));
        dto.put("gamePlayers", this.getGame().getGamePlayersList());
        dto.put("ships", getShipList());
        dto.put("salvoes",   this.getGame().getGamePlayers()
                                                    .stream()
                                                    .flatMap(gp -> gp.getSalvoes()
                                                            .stream()
                                                            .sorted(Comparator.comparing(Salvo::getTurn))
                                                            .map(salvo -> salvo.salvoDTO())
                                                            )
                                                    .collect(Collectors.toList())
                );
        dto.put("hits", Hitsdto(this,opponent));
        return dto;
    }

    private Map<String,Object> Hitsdto(GamePlayer self,
                                       GamePlayer opponent){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("self", getHits(self,opponent));
        dto.put("opponent", getHits(opponent,self));
        return dto;
    }

    public List<Map<String, Object>> getShipList() {
        { return this.getShips()
                     .stream()
                     .map(ship -> ship.makeshipDTO())
                     .collect(Collectors.toList());
        }
    }

    public String hasShips(){
        if (this.getShips().size() > 0){
            return  "YES";
        }else {
            return "NO";
        }

    }

    //***************************************DETERMINAR EL OPONENTE*****************************************************
    public Optional<GamePlayer> GetOpponent(){
        return this.getGame().getGamePlayers()
                .stream()
                .filter(opponent -> this.getId() != opponent.getId())
                .findFirst();
    }

    //**********************************CALCULAR A CUANTOS BARCOS DEL OPONENTE ACERTE***********************************
    public List<Map> getHits(GamePlayer self,
                              GamePlayer opponent){
        List<Map> dto = new ArrayList<>();

        int carrierDamage = 0;
        int destroyerDamage = 0;
        int patrolboatDamage = 0;
        int submarineDamage = 0;
        int battleshipDamage = 0;
        List<String> carrierLocations = new ArrayList<>();
        List<String> destroyerLocations = new ArrayList<>();
        List<String> submarineLocations = new ArrayList<>();
        List<String> patrolboatLocations = new ArrayList<>();
        List<String> battleshipLocations = new ArrayList<>();

        for (Ship ship: self.getShips()) {
            switch (ship.getType()){
                case "carrier":
                    carrierLocations = ship.getShipLocations();
                    break ;
                case "battleship" :
                    battleshipLocations = ship.getShipLocations();
                    break;
                case "destroyer":
                    destroyerLocations = ship.getShipLocations();
                    break;
                case "submarine":
                    submarineLocations = ship.getShipLocations();
                    break;
                case "patrol_boat":
                    patrolboatLocations = ship.getShipLocations();
                    break;
            }
        }
        List<Salvo> opponentSalvo = opponent.getSalvoes().stream().sorted(Comparator.comparing(Salvo::getTurn)).collect(Collectors.toList());
        for (Salvo salvo : opponentSalvo) {
            Integer carrierHitsInTurn = 0;
            Integer battleshipHitsInTurn = 0;
            Integer submarineHitsInTurn = 0;
            Integer destroyerHitsInTurn = 0;
            Integer patrolboatHitsInTurn = 0;
            Integer missedShots = salvo.getSalvoLocations().size();
            Map<String, Object> hitsMapPerTurn = new LinkedHashMap<>();
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();
            List<String> salvoLocationsList = new ArrayList<>();
            List<String> hitCellsList = new ArrayList<>();
            salvoLocationsList.addAll(salvo.getSalvoLocations());
            for (String salvoShot : salvoLocationsList) {
                if (carrierLocations.contains(salvoShot)) {
                    carrierDamage++;
                    carrierHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (battleshipLocations.contains(salvoShot)) {
                    battleshipDamage++;
                    battleshipHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (submarineLocations.contains(salvoShot)) {
                    submarineDamage++;
                    submarineHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (destroyerLocations.contains(salvoShot)) {
                    destroyerDamage++;
                    destroyerHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (patrolboatLocations.contains(salvoShot)) {
                    patrolboatDamage++;
                    patrolboatHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
            }


            damagesPerTurn.put("carrierHits", carrierHitsInTurn);
            damagesPerTurn.put("battleshipHits", battleshipHitsInTurn);
            damagesPerTurn.put("submarineHits", submarineHitsInTurn);
            damagesPerTurn.put("destroyerHits", destroyerHitsInTurn);
            damagesPerTurn.put("patrolboatHits", patrolboatHitsInTurn);
            damagesPerTurn.put("carrier", carrierDamage);
            damagesPerTurn.put("battleship", battleshipDamage);
            damagesPerTurn.put("submarine", submarineDamage);
            damagesPerTurn.put("destroyer", destroyerDamage);
            damagesPerTurn.put("patrolboat", patrolboatDamage);
            hitsMapPerTurn.put("turn", salvo.getTurn());
            hitsMapPerTurn.put("hitLocations", hitCellsList);
            hitsMapPerTurn.put("damages", damagesPerTurn);
            hitsMapPerTurn.put("missed", missedShots);
            dto.add(hitsMapPerTurn);
        }

        return dto;
    }

    private String getGameState(GamePlayer opponent) {
        Set<Ship> selfShips = this.getShips();
        Set<Salvo> selfSalvoes = this.getSalvoes();

        if (selfShips.size() == 0){
            return "PLACESHIPS";
        }
        if (opponent.getShips() == null){
            return "WAITINGFOROPP";
        }
        long turn = getCurrentTurn( opponent);
        Set<Ship> opponentShips = opponent.getShips();
        Set<Salvo> opponentSalvoes = opponent.getSalvoes();
        if (opponentShips.size() == 0){
            return "WAIT";
        }
        if(selfSalvoes.size() == opponentSalvoes.size()){
            Player selfPlayer = this.getPlayer();
            Game game = this.getGame();
            if (allPlayerShipsSunk(selfShips, opponentSalvoes) && allPlayerShipsSunk(opponentShips, selfSalvoes)){
                Score score = new Score(game,selfPlayer, 0.5f, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "TIE";
            }
            if (allPlayerShipsSunk(selfShips, opponentSalvoes)){
                Score score = new Score(game,selfPlayer, 0, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "LOST";
            }
            if(allPlayerShipsSunk(opponentShips, selfSalvoes)){
                Score score = new Score(game,selfPlayer, 1, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "WON";
            }
        }
        if (selfSalvoes.size() != turn){
            return "PLAY";
        }
        return "WAIT";
    }


    public long getCurrentTurn(GamePlayer opponent){
        int selfGPSalvoes = this.getSalvoes().size();
        int opponentGPSalvoes = opponent.getSalvoes().size();

        int totalSalvoes = selfGPSalvoes + opponentGPSalvoes;

        if(totalSalvoes % 2 == 0)
            return totalSalvoes / 2 + 1;

        return (int) (totalSalvoes / 2.0 + 0.5);
    }

    public  Boolean existScore(Score score, Game game){
        Set<Score> scores = game.getScores();
        for(Score s : scores){
            if(score.getPlayer().getUserName().equals(s.getPlayer().getUserName())){
                return true;
            }
        }
        return false;
    }

    private Boolean allPlayerShipsSunk(Set<Ship> selfShips,Set<Salvo> oppSalvoes ){
        Map<String,Object> damages = getDamages(selfShips, oppSalvoes);

        for (String key : damages.keySet()) {
            switch (key) {
                case "carrier":
                    damages.put("carrier", damages.get("carrier"));
                    break;
                case "submarine":
                    damages.put("submarine", damages.get("submarine"));
                    break;
                case "battleship":
                    damages.put("battleship", damages.get("battleship"));
                    break;
                case "destroyer":
                    damages.put("destroyer", damages.get("destroyer"));
                    break;
                case "patrolboat":
                    damages.put("patrolboat", damages.get("patrolboat"));
                    break;
            }
        }

        long selfSunkenShips = selfShips
                .stream()
                .filter(ship -> Long.parseLong(String.valueOf(damages.get(ship.getType()))) == ship.getShipLength(ship))
                .count();

        return selfSunkenShips == 5;

    }

    private Map<String,Object> getDamages(Set<Ship> selfShip,Set<Salvo> oppSalvoes) {
        Map<String, Object> dto = new LinkedHashMap<>();

        int carrierDamage = 0;
        int destroyerDamage = 0;
        int patrolboatDamage = 0;
        int submarineDamage = 0;
        int battleshipDamage = 0;
        List<String> carrierLocations = new ArrayList<>();
        List<String> destroyerLocations = new ArrayList<>();
        List<String> submarineLocations = new ArrayList<>();
        List<String> patrolboatLocations = new ArrayList<>();
        List<String> battleshipLocations = new ArrayList<>();

        for (Ship ship : selfShip) {
            switch (ship.getType()) {
                case "carrier":
                    carrierLocations = ship.getShipLocations();
                    break;
                case "battleship":
                    battleshipLocations = ship.getShipLocations();
                    break;
                case "destroyer":
                    destroyerLocations = ship.getShipLocations();
                    break;
                case "submarine":
                    submarineLocations = ship.getShipLocations();
                    break;
                case "patrolboat":
                    patrolboatLocations = ship.getShipLocations();
                    break;
            }
        }


        for (Salvo salvo : oppSalvoes) {
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();
            List<String> salvoShot = new ArrayList<>();
            salvoShot.addAll(salvo.getSalvoLocations());

            for (String salvoLocation : salvoShot) {
                if (carrierLocations.contains(salvoLocation)) {
                    carrierDamage++;
                }
                if (battleshipLocations.contains(salvoLocation)) {
                    battleshipDamage++;
                }
                if (submarineLocations.contains(salvoLocation)) {
                    submarineDamage++;
                }
                if (destroyerLocations.contains(salvoLocation)) {
                    destroyerDamage++;
                }
                if (patrolboatLocations.contains(salvoLocation)) {
                    patrolboatDamage++;
                }
            }

        }
        dto.put("carrier", carrierDamage);
        dto.put("battleship", battleshipDamage);
        dto.put("submarine", submarineDamage);
        dto.put("destroyer", destroyerDamage);
        dto.put("patrolboat", patrolboatDamage);
        return dto;
    }

}
