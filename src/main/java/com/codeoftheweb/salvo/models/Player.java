package com.codeoftheweb.salvo.models;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private String userName;

    @OneToMany(mappedBy="player", fetch=FetchType.EAGER)
    Set<GamePlayer> gamePlayers = new LinkedHashSet<>();

    @OneToMany(mappedBy="player", fetch=FetchType.EAGER)
    Set<Score> scores = new LinkedHashSet<>();

    private String password;

    public Player() {
    }

    public Player(String userName, String password)
    {
        this.userName = userName;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<GamePlayer> getGamePlayers() {
        return gamePlayers;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setGamePlayers(Set<GamePlayer> gamePlayers) {
        this.gamePlayers = gamePlayers;
    }

    public Set<Score> getScores() {
        return scores;
    }


    public float getWins(){
        return this.getScores().stream().filter(puntaje -> puntaje.getScore() == 1).count();
    }

    public float getLosses(){
        return this.getScores().stream().filter(puntaje -> puntaje.getScore() == 0).count();
    }
    public float getDraws(){
        return this.getScores().stream().filter(puntaje -> puntaje.getScore() == (float) 0.5).count();
    }

    public float getTotalscore (){
        float victorias = getWins()*1;
        float empates = getDraws()* (float) 0.5;
        float derrotas = getLosses() * 0;

        return victorias + empates + derrotas;
    }

    public Map<String,Object> playerDTO(){
        Map<String,Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", this.getId());
        dto.put("email", this.getUserName());
        return dto;
    }

    public Map<String, Object> playerLeaderBoardDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", this.getId());
        dto.put("userName", this.getUserName());
        dto.put("score", this.getScoreList());
        return dto;
    }

    public Map<String, Object> getScoreList () {
        Map<String,Object> dto = new LinkedHashMap<>();
        dto.put("total", this.getTotalscore());
        dto.put("won", this.getWins());
        dto.put("tied", this.getDraws());
        dto.put("lost", this.getLosses());
        return dto;
    }
    /*
    public void addGamePlayer(GamePlayer gamePlayer) {
        //gamePlayer.setPlayer(this);
        gamePlayers.add(gamePlayer);
    }

    public void addScore(Score score) {
        score.setPlayer(this);
        getScores().add(score);
    }

    */
}
