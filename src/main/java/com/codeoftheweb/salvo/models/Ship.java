package com.codeoftheweb.salvo.models;

import com.codeoftheweb.salvo.models.GamePlayer;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

@Entity
public class Ship {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private String type;

    @ElementCollection
    @Column(name="shipLocation")
    private List<String> shipLocations = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "gamePlayer_id")
    private GamePlayer gamePlayer;

    public Ship() {
    }

    public Ship(GamePlayer gamePlayer, String type, List<String> shipLocations) {
        this.type = type;
        this.shipLocations = shipLocations;
        this.gamePlayer = gamePlayer;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public List<String> getShipLocations() {
        return shipLocations;
    }

    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }

    public Map<String,Object> makeshipDTO(){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("shipType", this.getType());
        dto.put("shipLocations", this.getShipLocations());
        return dto;
    }

    public void setGamePlayer(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }

    public long getShipLength(Ship ship){
        long length = 0;
        switch (ship.getType()){
            case "carrier":
                length = 5;
                break ;
            case "battleship" :
                length = 4;
                break;
            case "submarine":
                length = 3;
                break;
            case "destroyer":
                length = 3;
                break;
            case "patrolboat":
                length = 2;
                break;
        }
        return length;
    }
}
